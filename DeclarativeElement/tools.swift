//
//  tools.swift
//  DeclarativeElement
//
//  Created by vikingosegundo on 02/12/2021.
//

import Foundation

func int   (from: Any?, missing:Int    = 0    ) -> Int    { from as? Int    ?? missing }
func bool  (from: Any?, missing:Bool   = false) -> Bool   { from as? Bool   ?? missing }
func string(from: Any?, missing:String = ""   ) -> String { from as? String ?? missing }

func perform(on: DispatchQueue = .main, _ execute: @escaping () -> ()) { on.async { execute() } }

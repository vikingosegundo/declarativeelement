//
//  Room.swift
//  DeclarativeElement
//
//  Created by vikingosegundo on 02/12/2021.
//

struct Room:Codable, Hashable {
    enum JoinRule:Codable, Hashable {
        case `public`
        case invite
    }
    enum Change {
        case name         (String  )
        case topic        (String  )
        case guestCanJoin (Bool    )
        case worldReadable(Bool    )
        case joinRule     (JoinRule)
        case numberMembers(Int     )
        case avatarURL    (String  )
    }
    let roomID       : String
    let name         : String
    let topic        : String
    let guestCanJoin : Bool
    let worldReadable: Bool
    let joinRule     : JoinRule
    let numberMembers: Int
    let avatarURL    : String
    
    init(roomID: String) { self.init(roomID, "", "", false, false, .public, 0, "") }
    private
    init(
        _ roomID       : String,
        _ name         : String,
        _ topic        : String,
        _ guestCanJoin : Bool,
        _ worldReadable: Bool,
        _ joinRule     : JoinRule,
        _ numberMembers: Int,
        _ avatarURL    : String
    ) {
        self.roomID        = roomID
        self.name          = name
        self.topic         = topic
        self.guestCanJoin  = guestCanJoin
        self.worldReadable = worldReadable
        self.joinRule      = joinRule
        self.numberMembers = numberMembers
        self.avatarURL     = avatarURL
    }
    func alter(_ changes:Change...) -> Self { changes.reduce(self) { $0.alter($1) } }
    private
    func alter(_ change :Change   ) -> Self {
        switch change {
        case let .name         (name)         : return .init(roomID, name, topic, guestCanJoin, worldReadable, joinRule, numberMembers, avatarURL)
        case let .topic        (topic)        : return .init(roomID, name, topic, guestCanJoin, worldReadable, joinRule, numberMembers, avatarURL)
        case let .guestCanJoin (guestCanJoin) : return .init(roomID, name, topic, guestCanJoin, worldReadable, joinRule, numberMembers, avatarURL)
        case let .worldReadable(worldReadable): return .init(roomID, name, topic, guestCanJoin, worldReadable, joinRule, numberMembers, avatarURL)
        case let .joinRule     (joinRule)     : return .init(roomID, name, topic, guestCanJoin, worldReadable, joinRule, numberMembers, avatarURL)
        case let .numberMembers(numberMembers): return .init(roomID, name, topic, guestCanJoin, worldReadable, joinRule, numberMembers, avatarURL)
        case let .avatarURL    (avatarURL)    : return .init(roomID, name, topic, guestCanJoin, worldReadable, joinRule, numberMembers, avatarURL)
        }
    }
}
func parseRooms(from dict: [String: Any], callback: @escaping ([Room]) -> ()) {
    perform(on:.global(qos:.default)) {
        perform {
            callback((dict["chunk"] as! [[String:Any]]).map {
                Room(roomID:string(from:$0["room_id"]))
                    .alter(
                        .name         (  string(from:$0["name"              ])),
                        .avatarURL    (  string(from:$0["avatar_url"        ])),
                        .numberMembers(     int(from:$0["num_joined_members"])),
                        .joinRule     (joinRule(from:$0["join_rule"         ])),
                        .topic        (  string(from:$0["topic"             ])),
                        .worldReadable(    bool(from:$0["world_readable"    ]))) } ) } }
}

fileprivate
func joinRule(from: Any?, missing:Room.JoinRule = .invite) -> Room.JoinRule {
    guard let from = from as? String else { return missing }
    switch from {
    case "public": return .public
    case "invite": return .invite
    default:
        return missing
    }
}

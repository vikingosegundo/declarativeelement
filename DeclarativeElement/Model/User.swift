//
//  User.swift
//  DeclarativeElement
//
//  Created by vikingosegundo on 30/11/2021.
//

struct Server:Codable {
    let baseURL:String
}

struct User:Codable {
    let name:String
    let accessToken:String
    let id:String
    
    let home:Server
}

//
//  RoomFetcher.swift
//  DeclarativeElement
//
//  Created by vikingosegundo on 30/11/2021.
//

func request(_ r: RoomFetcher.Request, from fetcher:RoomFetcher) { fetcher.request(r) }
struct RoomFetcher:UseCase {
    enum Request {
        case fetch(_Fetch); enum _Fetch {
            case all(_All); enum _All   {
                case rooms } }                  // .fetch(.all(.rooms))
    }
    enum Response{
        case fetching(_Fetching)              ; enum _Fetching {
            case all(_All)                    ; enum _All      {
                case rooms([Room], _Outcome) }; enum _Outcome  {
                    case succeeded,             // .fetching(.all(.rooms(.succeeded)))
                         failed(with: Error?) } // .fetching(.all(.rooms(.failed(with:error))))
        }
    }
    init(stack:MatrixStack, store: Store, responder: @escaping (Response) -> ()) {
        self.stack   = stack
        self.store   = store
        self.respond = responder
    }
    private let respond: (Response) -> ()
    private let store  : Store
    private let stack  : MatrixStack
    typealias  RequestType = Request
    typealias ResponseType = Response
}
        
fileprivate extension RoomFetcher {
    func request(_ change:Request) {
        switch change {
        case .fetch(.all(.rooms)): stack.fetchAllRooms {
            switch $0 {
            case let  .success(rooms): store.change(.add(.public(rooms))); respond(.fetching(.all(.rooms(rooms, .succeeded          ))))
            case let  .failure(error):                                     respond(.fetching(.all(.rooms([],    .failed(with: error)))))
            }
        }
        }
    }
}

//
//  createChatFeature.swift
//  DeclarativeElement
//
//  Created by vikingosegundo on 30/11/2021.
//

func createChatFeature(stack : MatrixStack,
                       store : Store,
                       output: @escaping Output) -> Input {
    let loginUseCase = LoginOut(stack:stack, store: store, responder:handle(output:output))
    let roomFetcher = RoomFetcher(stack:stack, store: store, responder:handle(output:output))
    return { msg in
        if case let .chat(.loginUser(named: name, password: pw)) = msg { request(.loginUser(named: name, password: pw), from: loginUseCase) }
        if case let .chat(.logoutUser(user)                    ) = msg { request(.logout(user),                         from: loginUseCase) }
        if case     .chat(.load(.public(.rooms))               ) = msg { request(.fetch(.all(.rooms)),                  from: roomFetcher ) }
    }
}

private func handle(output: @escaping Output) -> (   LoginOut.Response) -> () { { process(response:$0,output:output) } }
private func handle(output: @escaping Output) -> (RoomFetcher.Response) -> () { { process(response:$0,output:output) } }

private func process(response: LoginOut.Response, output: @escaping Output) {
    switch response {
    case let .loggedIn(.successfully(user)                         ): output(.chat(.loggedInUser(.successfully(user)))             )
    case let .loggedIn(.failedToLoginUser(named: name, with: error)): output(.chat(.loggedInUser(.failed(for: name, with: error))) )
    case     .loggedOut                                             : output(.chat(.userLoggedOut)                                 )
    }
}

private func process(response: RoomFetcher.Response, output: @escaping Output) {
    switch response {
    case let .fetching(.all(.rooms(rooms, .succeeded))): output(.chat(.loading(.public(.rooms(rooms, .succeeded)))))
    case let .fetching(.all(.rooms(_, .failed(with: error)))): output(.chat(.loading(.public(.rooms([], .failed(with: error))))))
    }
}

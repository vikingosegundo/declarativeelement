//
//  LoginOut.swift
//  DeclarativeElement
//
//  Created by vikingosegundo on 30/11/2021.
//

func request(_ r: LoginOut.Request, from login:LoginOut) { login.request(r) }
struct LoginOut:UseCase {
    enum Request {
        case loginUser(named:String, password:String)           // .loginUser(named:name, password:*****)
        case logout(User)                                       // .logout(user)
    }
    enum Response{
        case loggedIn(_Result); enum _Result {
            case successfully(User)                             // .loggedIn(.successfully(user))
            case failedToLoginUser(named:String, with:Error?) } // .loggedIn(.failedToLoginUser(named:name, with:error))
        case loggedOut                                          // .loggedOut
    }
    init(stack:MatrixStack, store: Store, responder: @escaping (Response) -> ()) {
        self.stack   = stack
        self.store   = store
        self.respond = responder
    }
    func request(_ request:Request) {
        switch request {
        case .loginUser(let named, let password):
            stack.login(userNamed: named, with: password) {
                switch $0 {
                case let .success(user): store.change(.loggedIn(user));respond(.loggedIn(.successfully(user)))
                case let .failure(error):store.change(.loggedIn(nil ));respond(.loggedIn(.failedToLoginUser(named: named, with: error)))
                }
            }
        case .logout(_): store.change(.loggedIn(nil), .add(.public([]))); respond(.loggedOut)
        }
    }
    private let respond: (Response) -> ()
    private let store  : Store
    private let stack  : MatrixStack
    typealias  RequestType  = Request
    typealias ResponseType = Response
}

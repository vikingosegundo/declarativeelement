//
//  AppState.swift
//  Todos
//
//  Created by Manuel Meyer on 12/09/2020.
//

func change(_ state: AppState, _ change: [ AppState.Change ] ) -> AppState { state.alter(change) }

struct AppState:Codable {
    enum Change {
        case loggedIn(User?)
        case add(_Add)
        
        enum _Add {
            case `public`([Room])
        }
    }
    
    
    private
    init(_ loggedInUser: User?, _ rooms:[Room])  {
        self.loggedInUser = loggedInUser
        self.rooms = rooms
    }
    
    init()  {
        self.init(nil, [])
    }
   
    let loggedInUser: User?
    let rooms: [Room]
    
    func alter(_ changes: [ Change ] ) -> AppState { changes.reduce(self) { $0.alter($1) } }
    
    private func alter (_ change:Change) -> Self {
        switch change {
        case let .loggedIn(loggedInUser): return .init(loggedInUser, rooms)
        case let .add(.public(rooms)): return .init(loggedInUser,rooms)
        }
    }
}

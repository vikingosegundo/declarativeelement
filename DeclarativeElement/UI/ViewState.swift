//
//  ViewState.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI

public
final class ViewState: ObservableObject  {

    @Published var loggedInUser:User?
    @Published var rooms:[Room] = []

    init(store: Store) {
        store.updated { self.process(state(in: store)) }
        process(state(in: store))
    }
    func handle(msg: Message) { }
    private func process(_ appState: AppState) {
        DispatchQueue.main.async {
            self.loggedInUser = appState.loggedInUser
            self.rooms        = appState.rooms
        }
    }
}

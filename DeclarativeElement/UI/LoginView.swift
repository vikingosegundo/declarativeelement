//
//  LoginView.swift
//  DeclarativeElement
//
//  Created by vikingosegundo on 02/12/2021.
//

import SwiftUI

struct LoginView:View {
    @State private var user  = "secondviking"
    @State private var password  = "3!TTXcpTGR"
    private let rootHandler: (Message) -> ()
    
    init(roothandler r: @escaping (Message) -> ()) { rootHandler = r }
    
    var body:some View {
        VStack {
            TextField  ("user",     text:$user    )
            SecureField("password", text:$password)
            Button {
                rootHandler(.chat(.loginUser(named:user, password:password)))
            } label: { Text("login") }.disabled(user.isEmpty || password.isEmpty)
        }.padding()
    }
}

//
//  ContentView.swift
//  DeclarativeElement
//
//  Created by vikingosegundo on 30/11/2021.
//

import SwiftUI

struct ContentView: View {
    init(viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.viewState   = viewState
        self.rootHandler = rootHandler
    }
    var body: some View {
        VStack {
            switch viewState.loggedInUser {
            case .none   : notLoggedIn
            case .some(_):    loggedIn
            }
        }.environmentObject(viewState)
    }
    @ObservedObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
}

private extension ContentView {
    var notLoggedIn:some View {
        login
    }
    var loggedIn:some View {
        VStack {
            userHeader
            channels
        }
    }
    var login: some View {
        LoginView(roothandler:rootHandler)
    }
    var userHeader: some View {
        HStack {
            Text(viewState.loggedInUser!.id)
            Button { rootHandler(.chat(.logoutUser(viewState.loggedInUser!))) } label: { Text("logout") }
        }
    }
    var channels: some View {
        ScrollView {
            LazyVStack {
                ForEach(viewState.rooms, id: \.roomID) { Text($0.name) }
            }
        }
    }
}


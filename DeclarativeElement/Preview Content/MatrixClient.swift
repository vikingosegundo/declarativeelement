//
//  MatrixClient.swift
//  DeclarativeElement
//
//  Created by vikingosegundo on 02/12/2021.
//

import Foundation

protocol Client {
    func loginUser  (named:String, with password:String, callback: @escaping (Result< User ,MatrixStack.Error>) -> ())
    func publicRooms(                                    callback: @escaping (Result<[Room],MatrixStack.Error>) -> ())
}

struct MatrixV3Client: Client {
    func loginUser(named: String, with password:String, callback: @escaping (Result<User, MatrixStack.Error>) -> ()) {
        guard
            let url = URL(string:"login", relativeTo:server),
            let data = try? JSONSerialization.data(withJSONObject: ["type":"m.login.password", "user":named, "password":password])
        else { return }
        var request = URLRequest(url:url)
        request.httpBody = data
        request.httpMethod = "POST"
        let task = URLSession(configuration:.default)
            .dataTask(with:request) { data, _, error in
            let dict = try! JSONSerialization.jsonObject(with: data!, options: []) as! [String : Any]
            switch error {
            case .some(_): callback(.failure(.unknown))
            case .none   : callback(.success(.init(name: named, accessToken: string(from:dict["access_token"]), id: string(from:dict["user_id"]), home: Server(baseURL: string(from:dict["home_server"])))))
            }
        }
        task.resume()
    }
    func publicRooms(callback: @escaping (Result<[Room], MatrixStack.Error>) -> ()) {
        guard
            let url = URL(string:"publicRooms", relativeTo:server)
        else { return  }
        let task = URLSession(configuration: .default).dataTask(with:URLRequest(url: url)) { data, _, error in
            let dict = try! JSONSerialization.jsonObject(with: data!, options: []) as! [String : Any]
            switch error {
            case .some(_): callback(.failure(.unknown))
            case .none   : parseRooms(from:dict) { callback(.success($0)) }
            }
        }
        task.resume()
    }
    private let server: URL = URL(string:"https://matrix.org/_matrix/client/v3/")!
}

//
//  createAppCore.swift
//  Minesweeper
//
//  Created by vikingosegundo on 25/11/2021.
//

import Foundation

typealias  Input = (Message) -> ()
typealias Output = (Message) -> ()

func createAppCore(
    stack:MatrixStack,
    store      : Store,
    receivers  : [Input],
    rootHandler: @escaping Output) -> Input
{
    store.updated {
#if targetEnvironment(simulator)
     //   print("\(state(in: store))")
#endif
    }
    let features: [Input] = [
        createChatFeature(stack:stack, store: store, output: rootHandler)
    ]
    return { msg in
        (receivers + features).forEach { $0(msg) }
    }
}


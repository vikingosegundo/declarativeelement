//
//  DeclarativeElementApp.swift
//  DeclarativeElement
//
//  Created by vikingosegundo on 30/11/2021.
//

import SwiftUI

@main final
class DeclarativeElementApp: App {
    private      let store: Store = createDiskStore()
    private      let stack = MatrixStack(client: MatrixV3Client())
    private lazy var viewState: ViewState = ViewState(store: store)
    private lazy var rootHandler: (Message) -> () = createAppCore(
        stack:stack,
        store:store,
        receivers:[ viewState.handle(msg:), expandMessages(rootHandler: { self.rootHandler($0) } ) ],
        rootHandler:{ self.rootHandler($0) }
    )
    var body: some Scene {
        WindowGroup {
            ContentView(viewState: viewState, rootHandler: rootHandler)
        }
    }
}

fileprivate
func expandMessages(rootHandler: @escaping (Message) -> ()) -> (Message) -> () {
    { msg in
        if case .chat(.loggedInUser(.successfully(_))) = msg { rootHandler(.chat(.load(.public(.rooms)))) }
    }
}

//
//  Message.swift
//  DeclarativeElement
//
//  Created by vikingosegundo on 30/11/2021.
//
enum Message {
    case chat(_Chat)
    
    enum _Chat {
        case loginUser(named:String, password:String)
        case loggedInUser(_User)
        case logoutUser(User)
        case userLoggedOut
        case load(_Load)
        case loading(_Loading)

        enum _Load {
            case `public`(_Public); enum _Public{
                case rooms
            }
        }
        enum _Loading {
            case `public`(_Public); enum _Public  {
                case rooms([Room], _Outcome)
            }
            enum _Outcome {
                case succeeded
                case failed(with: Error?)
            }
        }
        enum _User {
            case successfully(User)
            case failed(for:String, with: Error?)
        }
    }
}

//
//  UseCase.swift
//  Minesweeper
//
//  Created by vikingosegundo on 25/11/2021.
//

protocol UseCase {
    associatedtype RequestType
    associatedtype ResponseType
}

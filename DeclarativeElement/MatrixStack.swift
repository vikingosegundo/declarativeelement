//
//  MatrixStack.swift
//  DeclarativeElement
//
//  Created by vikingosegundo on 30/11/2021.
//

import Foundation
import UIKit

final class MatrixStack {
    enum Error: E {
        case unknown
    }
    init(client c: Client) {
        client = c
    }
    func login(userNamed:String, with password:String, callback: @escaping (Result<User, MatrixStack.Error>) -> ()) {
        client.loginUser(named: userNamed, with: password, callback: callback)
    }
    func fetchAllRooms(callback: @escaping (Result<[Room], MatrixStack.Error>) -> ()) {
        client.publicRooms(callback: callback)
    }
    private
    let client:Client
}

typealias E = Error
